﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickChat.WebServiceLib
{
    /// <summary>
    /// Stores temporary hardcoded configuration values.
    /// Ideally, this would be retrieved over I/O.
    /// </summary>
    public class FixedConfiguration : IGlobalConfiguration
    {
        public CultureInfo GetCultureInfo()
        {
            var culture = new CultureInfo("en-US");
            return culture;
        }
    }
}
