﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickChat.WebServiceLib
{
    public interface IUserManager
    {
        User GetUserFromID(Guid userID);
        Guid CreateNewUser(string firstName, string middleName, string lastName);
        bool DeleteUser(Guid userToDelete, IConversationManager conversationManager);
    }
}
