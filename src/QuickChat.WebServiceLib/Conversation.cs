﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickChat.WebServiceLib
{
    public class Conversation
    {
        public List<Message> MessagesInConversation { get; private set; }
        public List<Guid> UsersInConversation { get; private set; }
        public Guid ConversationID { get; private set; }

        public Conversation(List<Guid> users, Guid conversationID)
        {
            ConversationID = new Guid();
            MessagesInConversation = new List<Message>();
            UsersInConversation = users;
        }

        public void AddUserToConversation(Guid userToAdd)
        {
            UsersInConversation.Add(userToAdd);
        }

        public bool RemoveUserFromConversation(Guid userToRemove)
        {
            return UsersInConversation.Remove(userToRemove);
        }

        public void AddMessage(Message messageToSend)
        {
            MessagesInConversation.Add(messageToSend);
        }
    }
}
