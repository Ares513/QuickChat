﻿using Newtonsoft.Json;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

//using https://stackoverflow.com/questions/4015324/how-to-make-http-post-web-request
//and https://stackoverflow.com/questions/27682896/httpclient-asynchronous-post-c-sharp

namespace QuickChat.WebServiceLib
{
    public class FixedDataTransmitter : IDataTransmitter
    {
       // string FilePath { get; set; }

        private static readonly HttpClient client = new HttpClient();

        public FixedDataTransmitter(/*string filePath*/)
        {
            //FilePath = filePath;
        }

        public async Task SendMessage(Message messageToSend, Guid conversationToSendMessageTo)
        {
            List<Message> listOfMessages = new List<Message>
            {
                messageToSend
            };
            string output = JsonConvert.SerializeObject(listOfMessages);
            //File.WriteAllText(FilePath, output);

            StringContent payload = new StringContent(output);
            Console.WriteLine(output);
            try
            {
                var response = await client.PostAsync("http://quickchatserversolutions.azurewebsites.net/api/values", payload);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return;
        }

        public async Task SendUserInformation(List<User> usersToSend)
        {
            string output = JsonConvert.SerializeObject(usersToSend);
            //File.WriteAllText(FilePath, output);

            StringContent payload = new StringContent(output);
            var response = await client.PostAsync("http://www.sample.com/write", payload);
            return;
        }

        public async Task SendConversation(Conversation conversationToSend)
        {
            string output = JsonConvert.SerializeObject(conversationToSend);
            //File.WriteAllText(FilePath, output);

            StringContent payload = new StringContent(output);
            var response = await client.PostAsync("http://www.sample.com/write", payload);
            return;
        }
    }
}
