﻿using System;
using System.Collections.Generic;

namespace QuickChat.WebServiceLib
{
    public class User
    {
        public Guid UserID { get; private set; }
        public bool OnlineStatus { get; private set; }
        public List<Guid> Conversations { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string MiddleName { get; private set; }

        public User(string lastName, string firstName, string middleName, Guid Id, bool onlineStatus, List<Guid> conversations)
        {
            UserID = new Guid();
            OnlineStatus = onlineStatus;
            LastName = lastName;
            FirstName = firstName;
            MiddleName = middleName;
            Conversations = conversations;
        }

        public void AddConversationToMyList(Guid conversationIdToAdd)
        {
            Conversations.Add(conversationIdToAdd);
        }

        public void RemoveConversationFromMyList(Guid conversationIDToRemove)
        {
            Conversations.Remove(conversationIDToRemove);
        }
    }
}
