﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickChat.WebServiceLib
{
    public interface IServerCommunicationHandler
    {
        List<User> GetAllUsers();
    }
}
