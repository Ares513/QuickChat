﻿using Microsoft.EntityFrameworkCore;
using QuickChat.WebServiceLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quickchat.Server.Models
{

    public class MessagesContext : DbContext
    {
        public MessagesContext(DbContextOptions<MessagesContext> options)
            : base(options)
        {
        }

        public DbSet<Message> Messages { get; set; }

    }
}
