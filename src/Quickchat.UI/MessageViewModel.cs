﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quickchat.UI
{
    public class MessageViewModel
    {
        private DateTime _timestamp;

        public string MessageTimestamp
        {
            get
            {
                if (DateTime.Now.Date > _timestamp.Date)
                {
                    //we only want to show the full date on days that aren't today
                    return _timestamp.ToString("d t");
                }
                return _timestamp.ToString("t");
            }
        }

        public string From { get; set; }

        public string Message { get; set; }

        public MessageViewModel(DateTime timestampToRender)
        {
            _timestamp = timestampToRender;
        }
    }
}

