﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimMetricsMetricUtilities;

namespace Quickchat.UI
{
    public class LocalNameContactSearch : IContactSearchEngine
    {
        public List<ContactViewModel> SearchContactViewModels(List<ContactViewModel> contacts, string search)
        {

            var prefiltered = contacts.Where(contact => 
            CultureSensitiveCaseInsensitiveContains(contact.FirstName, search) ||
            CultureSensitiveCaseInsensitiveContains(contact.LastName, search));

            var distanceComputation = new Jaro();
    
            var similarity = prefiltered.OrderByDescending(
                    contact => distanceComputation.GetSimilarity(contact.FirstName + " " + contact.LastName, search));
            return similarity.Take(10).ToList();
        }

        //While it might seem obvious to use ToLower(), this a) allocates a lot more memory, since the string must be copied
        //and b) is culture insensitive- you can only answer the question 'are these two strings the same but in different cases'
        //if you know the language the text is in.
        private bool CultureSensitiveCaseInsensitiveContains(string first, string second) =>
            CultureInfo.CurrentCulture.CompareInfo.IndexOf(first, second, CompareOptions.IgnoreCase) >= 0;
        
    }
}
