﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;

namespace Quickchat.UI
{
    public class FixedLanguageMap : ILanguageMap
    {
        public Run GetEmptyMessageInputText()
        {
            return new Run("Enter your message here...") {FontStyle = FontStyles.Italic};
        }
    }
}
